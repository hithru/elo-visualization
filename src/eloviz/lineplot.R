lineplot=function(expd, expr, gene, sel.group){
  #Create a line plot using the sample information in "expd" and the expression data in "expr"
  #"sel.group" is the variable in "expd" that is used to separate the samples on the x axis
  #We average all replicates (rows that have the same values for time, variety and treatment)
  
  qm=match(c("time.order", "Variety", "Treatment", "time.label"), colnames(expd))
  qm=qm[!is.na(qm)]
  group.data=expd[,qm]
  group.vars=colnames(group.data)
 
  to.plot=cbind(tibble(y=expr), group.data)
  #Average replicates
  to.plot=group_by(to.plot, .dots=group.vars) %>% summarize(y=mean(y)) %>% ungroup()
  to.plot=arrange_at(to.plot, .vars=group.vars)
  
  if(sel.group=="None"){
    #Drop time.order, since we will be showing only time label on plot
    if("time.order" %in% group.vars){
      qw=which(group.vars!="time.order")
      group.vars=group.vars[qw]
      to.plot=to.plot %>% dplyr::select(-time.order)
    }
    to.plot$x=apply(to.plot[,group.vars], 1, paste, collapse=" | ")
    
    ggplot(data=to.plot, aes(x=1:nrow(to.plot),
                             y=y))+
      geom_line()+ylab(gene)+
      scale_x_discrete(name="",
                       limits=1:nrow(to.plot),
                       breaks=1:nrow(to.plot),
                       labels=to.plot$x)+
      theme(axis.text.x = element_text(angle=45,hjust = 1))
    
  }else if(sel.group=="Time"){
    other.groups=setdiff(group.vars, c("time.order", "time.label"))
    time.vals=unique(to.plot[,c("time.order", "time.label")])
    time.vals=arrange(time.vals, time.order)
    if(length(other.groups)==0){
      ggplot(data=to.plot, aes_string(x="time.order", 
                                      y="y"))+
        geom_line()+ylab(gene)+
        scale_x_discrete(name="Time",
                         limits=time.vals$time.order,
                         breaks=time.vals$time.order,
                         labels=time.vals$time.label)
      
    }else if(length(other.groups)==1){
      ggplot(data=to.plot, aes_string(x="time.order", 
                                      y="y", 
                                      group=other.groups, 
                                      color=other.groups))+
        geom_line()+ylab(gene)+
        scale_x_discrete(name="Time",
                         limits=time.vals$time.order,
                         breaks=time.vals$time.order,
                         labels=time.vals$time.label)
    }else{
      to.plot$Other_vars=apply(to.plot[,other.groups], 1, paste, collapse=" | ")
      ggplot(data=to.plot, aes_string(x="time.order", 
                                      y="y", 
                                      group="Other_vars", 
                                      color="Other_vars"))+
        geom_line()+ylab(gene)+
        scale_x_discrete(name="Time",
                         limits=time.vals$time.order,
                         breaks=time.vals$time.order,
                         labels=time.vals$time.label)
    }
  }else{
    if("time.order" %in% group.vars){
      qw=which(group.vars!="time.order")
      group.vars=group.vars[qw]
      to.plot=to.plot %>% dplyr::select(-time.order)
    }    

    other.groups=setdiff(group.vars, sel.group)
    if(length(other.groups)>1){
        to.plot$Other_vars=apply(to.plot[,other.groups], 1, paste, collapse=" | ")
        ggplot(data=to.plot, aes_string(x=sel.group, 
                                        y="y", 
                                        group="Other_vars", 
                                        color="Other_vars"))+
          geom_line()+ylab(gene)
    }else if(length(other.groups)==1){
        ggplot(data=to.plot, aes_string(x=sel.group, 
                                        y="y", 
                                        group=other.groups, 
                                        color=other.groups))+
          geom_line()+ylab(gene)
    }else{
      ggplot(data=to.plot, aes_string(x=sel.group, 
                                      y="y"))+
        geom_line()+ylab(gene)
    }
  }
}
