#Start an AWS EC2 instance 
#Step 1: Create an EC2 instance
#Log in to AWS account, click on EC2 under the ‘Compute’ header or click on EC2 under ‘Recently visited #services’
#Click on Launch Instance

#Choose a Machine Image and Instance type (I used m5n.large with Ubuntu 16.04 HVM)

#Click on Edit security groups -> Configure security group
#In the SSH row, change source to ‘My IP’
#Click on add a Rule, custom TCP Rule would be added. Under the ‘Port range’ enter 3838. This is the port
#for R shiny server. Set "Source" to "Anywhere" so anyone can access the Shiny server
##Note: for connecting Shiny to port 80 (so someone doesn't need to specify the port when accessing the app
#do the same thing for port 80. Then update "listen 80;" in /etc/shiny-server/shiny-server.conf
##To check with ports are open from within a machine use "netstat -ntlp" or "netstat -nulp"

#Click ‘Review and Launch’ and then click ‘Launch’. Upon doing this you will get a dialogue box like 
#below. The dialogue box helps in creating a private key which will enable us to ssh into the EC2
#instance. Give a name (hithru-elo.pem) to the key and click ‘Download Key Pair’. You will get .pem file. 
#Save the .pem file in ~/.ssh directory of local machine

#After the instance has started, log into it ("ec2-3-92-92-106.compute-1.amazonaws.com" from the EC2 console)
ssh -i <pem file path> ubuntu@ec2-3-92-92-106.compute-1.amazonaws.com

#For Elo owned server (deployment server)
ssh -i ~/.ssh/Sudhir-pem.pem ubuntu@ec2-3-233-232-22.compute-1.amazonaws.com

#Install R
sudo apt-get update
sudo apt-get install r-base
sudo apt-get install r-base-dev

#Upgrade R
sudo apt-get update
sudo apt-get install gdebi-core

export R_VERSION=3.5.1

curl -O https://cdn.rstudio.com/r/ubuntu-1604/pkgs/r-${R_VERSION}_1_amd64.deb
sudo gdebi r-${R_VERSION}_1_amd64.deb

/opt/R/${R_VERSION}/bin/R --version

sudo ln -s /opt/R/${R_VERSION}/bin/R /usr/bin/R
sudo ln -s /opt/R/${R_VERSION}/bin/Rscript /usr/bin/Rscript


#Install shiny, shinydashboard and DT from Rstudio
sudo su - -c "R -e \"install.packages(c('shiny', 'shinydashboard', 'DT'), repos='http://cran.rstudio.com/')\""

#Install tidyverse
sudo apt install libcurl4-openssl-dev libssl-dev libxml2-dev
sudo su - -c "R -e \"install.packages('tidyverse', repos='http://cran.rstudio.com/')\""

#Install rmarkdown (Important for reading rmd files!) and others
sudo su - -c "R -e \"install.packages(c('rmarkdown', 'shinythemes', 'shinyWidgets', 'shinyjs'), repos='http://cran.rstudio.com/')\""

#Install d3heatmap from source *NOT USING*
#sudo apt install libpng-devel
#sudo su - -c "R -e \"install.packages(c('png', 'dendextend'), repos='http://cran.rstudio.com/')\""
#curl -O https://cran.r-project.org/src/contrib/Archive/d3heatmap/d3heatmap_0.6.1.2.tar.gz 
#sudo su - -c "R -e \"install.packages('/home/ubuntu/d3heatmap_0.6.1.2.tar.gz', repos=NULL, type='source')\""

#Install shinybusy and others
#On ELO's AWS server I had to install "bitops"
sudo su - -c "R -e \"install.packages('bitops', repos='http://cran.rstudio.com/')\""
#then install "caTools" from source
curl -O https://cran.r-project.org/src/contrib/Archive/caTools/caTools_1.17.tar.gz
sudo su - -c "R -e \"install.packages('/home/ubuntu/caTools_1.17.tar.gz', repos=NULL, type='source')\""

#Needed to install data.table, which is needed for plotly
sudo apt install pkg-config

#Is this necessary?
sudo su - -c "R -e \"install.packages('pkgconfig', repos='http://cran.rstudio.com/')\""

sudo su - -c "R -e \"install.packages('data.table', repos='http://cran.rstudio.com/')\""

sudo su - -c "R -e \"install.packages(c('gplots', 'plotly', 'seriation'), repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages(c('shinybusy', 'RColorBrewer', 'shinyBS', 'heatmaply'), repos='http://cran.rstudio.com/')\""

#IMPORTANT!! Make sure that tidyselect version is >=1.0.0 (need function "all_of")
#Otherwise reinstall tidyselect
#sudo su - -c "R -e \"install.packages('tidyselect', repos='http://cran.rstudio.com/')\""


#Update version of httpuv to solve problem with NGINX settings
#sudo su - -c "R -e \"install.packages('remotes', repos='http://cran.rstudio.com/')\""

#sudo su - -c "R -e \"remotes::install_github('rstudio/httpuv')\""


#Install bsplus
#sudo su - -c "R -e \"install.packages('bsplus', repos='http://cran.rstudio.com/')\""

#Install AnnotationDBI
sudo su - -c "R -e \"
if (!requireNamespace('BiocManager', quietly = TRUE))
    install.packages('BiocManager', repos='http://cran.rstudio.com/')
\""

sudo su - -c "R -e \"BiocManager::install(c('AnnotationDbi', 'Category', 'gage', 'pathview', 'minet'))\""

#Create annotation packages
#See "make.annotation.package.R" for how to make annotation packages for a given genome
#Install chickpea annotation package
sudo su - -c "R -e \"install.packages('/home/ubuntu/org.Carietinum.eg.db_0.1.tar.gz', repos=NULL, type='source')\""
#Install banana annotation package
sudo su - -c "R -e \"install.packages('/home/ubuntu/org.Macuminata.eg.db_0.1.tar.gz', repos=NULL, type='source')\""

#Install other packages
sudo su - -c "R -e \"install.packages(c('shinyalert', 'infotheo', 'networkD3', 'igraph'), repos='http://cran.rstudio.com/')\""

#shinyTree and VennDiagram
sudo su - -c "R -e \"install.packages(c('shinyTree', 'VennDiagram'), repos='http://cran.rstudio.com/')\""

#install TnT (for genome browser) had to install from source since latest package needs R>3.4
sudo su - -c "R -e \"BiocManager::install('TnT')\""

sudo su - -c "R -e \"install.packages('/home/ubuntu/TnT_1.10.0.tar.gz', repos=NULL, type='source')\""

#Install Shiny
#Once you’ve installed R and the Shiny package, execute the following
#commands in a terminal window to install gdebi (which is used to
#install Shiny Server and all of its dependencies) and Shiny Server.
sudo apt-get install gdebi-core
wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.12.933-amd64.deb
sudo gdebi shiny-server-1.5.12.933-amd64.deb

#Setup the Server for Secure Package Installation
#R packages can be installed securely using an HTTPS CRAN mirror, or
#insecurely using an HTTP mirror. Beginning with R version 3.2.2, 
#HTTPS is the default preference when installing packages, but older 
#versions of R default to insecure HTTP. You can change this behavior
#in older versions of R by setting the download.file.method option 
#in your .Rprofile. See R Startup Files for details on where these 
#files are located.
#You should add the following line to Rprofile.site, to configure 
#how all users install packages, or the ~/.Rprofile file for 
#individual users who will be running Shiny applications in Shiny Server.
#For R 3.2

#Add the following line to sudo vi /opt/R/3.5.1/lib/R/etc/Rprofile.site ("/opt/R/3.5.1/lib/R" found by running "R.home()" in R) #Had to create a new file
options(download.file.method = "libcurl")


#To connect using SFTP with Filezilla
#First install puttygen
sudo apt install putty-tools
#Convert pem file to ppk
puttygen hithru-elo.pem -O private -o hithru-elo.ppk
#In Filezilla, File->Site Manager->New Site
#Add the EC2 URL "ec2-3-92-92-106.compute-1.amazonaws.com" and logon type "key file" with user "ubuntu" and key file selected as "hithru-elo.ppk"
#Rename site to "ELO-AWS" and click OK
#To connect, click on the site dropdown (top leftmost) and select "ELO-AWS"

###Creating a project in Rstudio
#Open Rstudio and click on new project. Select "Existing directory" and then "ELO-visualization" directory
#Turn on version control Tools->Project options->Git/SVN. Select "git" from dropdown menu and press OK


###Added users "sudhir" and "amrit" with no passwords
#Use the adduser command to add a new user account to an EC2 instance without a password
sudo adduser <newuser> --disabled-password

#Add user to sudo group
usermod –aG sudo <newuser>

#Change the security context to the new_user account so that folders and files you create will have the correct permissions:
sudo su - new_user

#Create a .ssh directory in the new_user home directory:
mkdir .ssh

#Use the chmod command to change the .ssh directory's permissions to 700.
#Changing the permissions restricts access so that only the new_user can read, write, or open the .ssh directory.
chmod 700 .ssh

#Use the touch command to create the authorized_keys file in the .ssh directory:
touch .ssh/authorized_keys

#Use the chmod command to change the .ssh/authorized_keys file permissions to 600.
chmod 600 .ssh/authorized_keys

#Get the public key from the pem file
ssh-keygen -y -f <pem file path>
#Add public key to authorized_keys
cat >> .ssh/authorized_keys #Paste in public key and press Ctrl-D

#I created another key pair ("amrit-key.pem") for Amrit and pasted the public key for that
#key pair into .ssh/authorized_keys in his directory


#Update the following in /etc/shiny-server/shiny-server.conf
	##Note: for connecting Shiny to port 80 (so someone doesn't need to specify the port when accessing the app
	#do the same thing for port 80. Then update "listen 80;" in /etc/shiny-server/shiny-server.conf

	# Host the directory of Shiny Apps stored in this directory
	site_dir /srv/shiny-server/eloviz;

	# When a user visits the base URL rather than a particular application,
	# an index of the applications available in this directory will be shown.
	directory_index off;
#Then restart shiny-server


#Deploy to EC2. chowning to "shiny:shiny" is important for shiny 
#to be able to write files to the server directory
sudo systemctl stop shiny-server
sudo rm -rf /srv/shiny-server/eloviz
sudo mv eloviz /srv/shiny-server/
sudo chown -R shiny:shiny /srv/shiny-server/eloviz
sudo systemctl start shiny-server

#Shiny logs
sudo ls -lt /var/log/shiny-server

#Show "tail" of latest file in /var/log/shiny-server
sudo tail -- "$(find /var/log/shiny-server -maxdepth 1 -type f -printf '%T@.%p\0' | sort -znr -t. -k1,2 | while IFS= read -r -d '' -r record ; do printf '%s' "$record" | cut -d. -f3- ; break ; done)"

#Install shiny server pro
#Remove shiny server
sudo apt-get remove shiny-server
#Next, download the appropriate package for Shiny Server Pro and install it using your package manager:
#sudo apt-get install gdebi-core#Already done
wget https://s3.amazonaws.com/rstudio-shiny-server-pro-build/ubuntu-14.04/x86_64/shiny-server-commercial-1.5.13.1042-amd64.deb
sudo gdebi shiny-server-commercial-1.5.13.1042-amd64.deb

#Create Certificate Signing Request (CSR) for HTTPS certificate
#Create key pair
sudo openssl genrsa -out /etc/shiny-server/server.key 2048
#Create CSR
sudo openssl req -new -key /etc/shiny-server/server.key -out /etc/shiny-server/server.csr
#Use the following options
#Country Name (2 letter code) [AU]:US
#State or Province Name (full name) [Some-State]:North Carolina
#Locality Name (eg, city) []:Research Triangle Park
#Organization Name (eg, company) [Internet Widgits Pty Ltd]:Elo Life Systems
#Organizational Unit Name (eg, section) []:
#Common Name (e.g. server FQDN or YOUR name) []:analytics.elolife.ag
#Email Address []:zack.miller@precisionbiosciences.com
#A challenge password []:
#An optional company name []:


###RStudio Connect#
#Install Connect
curl -Lo rsc-installer.sh https://cdn.rstudio.com/connect/installer/installer-v1.1.0.sh
#dpkg-sig is needed
sudo apt install dpkg-sig
sudo bash ./rsc-installer.sh 1.8.4-11
#Add the following to /etc/rstudio-connect/rstudio-connect.gcfg

[Server]
Address = "http://analytics.elolife.ag"

[HTTPS]
Listen = ":443"
Permanent = "true"
Certificate = "/etc/rstudio-connect/58fdecca6341bc43.crt"
Key = "/etc/rstudio-connect/server.key"

[SAML]
Logging = "true"
IdPMetaData = https://precisionbio.okta.com/app/exk8nngrtwHlYWfax2p7/sso/saml/metadata
IdPAttributeProfile = Okta
SSOInitiated = IdPAndSP
GroupsAutoProvision = true

#Tell RStudio to use the local versions of some packages
#sudo vi /etc/rstudio-connect/rstudio-connect.gcfg
#Add the following
[Packages]
External = Matrix
External = bitops
External = caTools
External = org.Carietinum.eg.db
External = org.Macuminata.eg.db

#Connecting to Connect through RStudio
#Get the SSL cert info from R
system("echo | openssl s_client -connect analytics.elolife.ag:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'")
#Copy and append the entire certificate text to /etc/ssl/certs/ca-certificates.crt
#Connect to "https://analytics.elolife.ag", using "Manage connections" in RStudio

#To deploy the app
#Add the BioConductor repo to the list of repos in R
options(repos = c("CRAN"="https://cloud.r-project.org", BiocManager::repositories()))
#Use the "Publish" button

#Install a couple of Linux packages needed by two R packages
sudo apt install libfontconfig1-dev
sudo apt install libcairo2-dev

